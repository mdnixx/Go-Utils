package main

import (
	"log"
	"os"
	"os/exec"
	"strings"
)

func main() {
	lf, err := os.OpenFile("messages.log", os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0640)
	if err != nil {
		log.Fatalln("Failed to create logfile")
	}

	log.SetOutput(lf)

	cmd := "oc"
	args := []string{"get", "pods", "-n", "mgt-oauth-proxy", "-o", "template", "--template='{{range .items}}{{.metadata.name}}#{{end}}'"}

	out, err := exec.Command(cmd, args...).Output()
	if err != nil {
		log.Fatal("Unable to get running fluentd pods.")
	}

	pb := string(out)
	pb = strings.Trim(pb, "'")
	pb = pb[:len(pb)-1]

	pods := strings.Split(pb, "#")

	for _, p := range pods {
		cmd := "oc"
		args := []string{"logs", "-n", "mgt-oauth-proxy", "--tail=10", p}
		out, err := exec.Command(cmd, args...).Output()
		if err != nil {
			log.Fatal("Unable to read logs of fluetd pods.")
		}

		if strings.Contains(string(out), "error") {
			args := []string{"delete", "pod", "-n", "mgt-oauth-proxy", p}
			out, err := exec.Command(cmd, args...).Output()
			if err != nil {
				log.Fatal("An error occured while deleting", p, out, err)
			}

			log.Printf("Pod %s has been deleted due to errors.\n", p)
		} else {
			log.Printf("Pod %s status OK",p)
		}

	}
	log.Printf("Nothing to delete. 0 errors...\n")

}
